/*
 * Copyright (C) 2009-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

/**
 * @brief Radio Aided Satellite Navigation Technique functions
 * @author Tobias Lorenz <tobias.lorenz@gmx.net>
 *
 * This file contains functions for Radio Aided Satellite Navigation Technique handling.
 */

#pragma once

#include <cstdint>
#include <ISO/62106.h>

namespace RASANT {

class Rasant {
public:
    /**
     * Constructor
     *
     * @param rdsProgram Link to parent
     */
    Rasant(ISO62106::RdsProgram * rdsProgram);

    /**
     * @brief Callback function
     *
     * This variable contains the callback function.
     *
     * @param[in,out] _rs RDS program
     * @param[in] _data RTCM data
     */
    void (*callback)(ISO62106::RdsProgram * _rs, uint8_t _data);

    /** @brief Decode and handle RASANT message
     *
     * This function decodes and handles RASANT messages.
     *
     * @param[in] _x Block 2 data
     * @param[in] _y Block 3 data
     * @param[in] _z Block 4 data
     */
    void decodeA(uint8_t _x, uint16_t _y, uint16_t _z);

private:
    ISO62106::RdsProgram * rdsProgram;

    /**
     * @brief Parity function
     *
     * This functions return the parity information for the given data.
     *
     * @param[in] _th Data
     * @return        Parity information
     */
    uint8_t isgpsParity(uint32_t _th);

    /**
     * @brief RTCM stream generation function
     *
     * This functions generated the RTCM stream based on RTCM blocks.
     *
     * @param[in] len Length
     * @param[in] ip  Pointer to RTCM block
     */
    void rtcmStream(uint8_t len, uint32_t *ip);

    /**
     * @brief RTCM message
     *
     * This variable contains the RTCM message content.
     */
    uint32_t rtcm[8];

    /**
     * @brief Sequence number
     *
     * This variable contains the RTCM sequence number.
     */
    uint32_t sqnum;

    /**
     * @brief Differential GPS Corrections
     *
     * This function generates RTCM msg1 blocks.
     */
    void rtcm2Msg1(void);

    /**
     * @brief Delta Differential GPS Corrections
     *
     * This function generates RTCM msg2 blocks.
     */
    void rtcm2Msg2(void);

    /**
     * @brief Reference Station Parameters
     *
     * This function generates RTCM msg3 blocks.
     */
    void rtcm2Msg3(void);

    /**
     * @brief Reference Station Datum
     * @todo not necessary?
     *
     * This function generates RTCM msg4 blocks.
     */
    void rtcm2Msg4(void);

    /**
     * @brief Constellation Health
     * @todo not necessary?
     *
     * This function generates RTCM msg5 blocks.
     */
    void rtcm2Msg5(void);

    /**
     * @brief Null Frame
     * @todo not necessary?
     *
     * This function generates RTCM msg6 blocks.
     */
    void rtcm2Msg6(void);

    /**
     * @brief Radio Beacon Almanac
     * @todo not necessary?
     *
     * This function generates RTCM msg7 blocks.
     */
    void rtcm2Msg7(void);

    /**
     * @brief Partial Satellite Set Differential Corrections
     *
     * This function generates RTCM msg9 blocks.
     */
    void rtcm2Msg9(void);

    /**
     * @brief Special Message
     *
     * This function generates RTCM msg5 blocks.
     */
    void rtcm2Msg16(void);
};

}
